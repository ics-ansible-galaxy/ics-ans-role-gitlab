---
- name: install required dependencies
  yum:
    name:
      - pygpgme
      - yum-utils
    state: present

- name: set swappiness to 10 to make the most of the RAM
  sysctl:
    name: vm.swappiness
    value: "10"
    state: present

- name: install the GitLab yum repository
  copy:
    src: "gitlab_gitlab-{{ gitlab_edition }}.repo"
    dest: /etc/yum.repos.d/
    owner: root
    group: root
    mode: 0644

- name: install GitLab
  yum:
    name: "gitlab-{{ gitlab_edition }}{{ '-' + gitlab_version if gitlab_version != 'latest' else '' }}"
    state: "{{ 'present' if gitlab_version != 'latest' else 'latest' }}"

- name: create GitLab configuration
  template:
    src: gitlab.rb.j2
    dest: /etc/gitlab/gitlab.rb
    owner: root
    group: root
    mode: 0600
  notify: reconfigure gitlab

- name: add trusted certificates
  copy:
    content: "{{ item.certificate }}"
    dest: "/etc/gitlab/trusted-certs/{{ item.name }}.crt"
    owner: root
    group: root
    mode: 0644
  loop: "{{ gitlab_trusted_certificates }}"
  no_log: true
  notify: reconfigure gitlab

- name: configure the crontab to run backup every night
  cron:
    name: "Backup GitLab"
    minute: "5"
    hour: "2"
    job: "/opt/gitlab/bin/gitlab-rake gitlab:backup:create SKIP=registry,artifacts CRON=1 > /tmp/gitlab-backup.log 2>&1"

- name: configure the crontab to run container registry garbage collection every night
  cron:
    name: "Container registry garbage collection"
    minute: "5"
    hour: "5"
    job: "/opt/gitlab/bin/gitlab-ctl registry-garbage-collect -m > /tmp/gitlab-registry-garbage-collect.log 2>&1"

- name: force all notified handlers to run
  meta: flush_handlers

- name: check that we can connect to GitLab web ui
  uri:
    url: "{{ gitlab_external_url }}"
    return_content: true
    validate_certs: "{{ gitlab_validate_certs }}"
  register: gitlab_web_request
  until: '"<meta content=\"GitLab" in gitlab_web_request.content'
  retries: 10
  delay: 10

- name: Install elasticsearch
  include_role:
    name: ics-ans-role-elasticsearch
  when: gitlab_elasticsearch | bool
