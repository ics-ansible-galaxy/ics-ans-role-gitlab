import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("ics-ans-role-gitlab-ee")


def test_gitlab_ee_installed(host):
    package = host.package("gitlab-ee")
    assert package.is_installed
    assert package.version == "15.0.4"


def test_daemon_running(host):
    assert host.socket("tcp://127.0.0.1:9200")
    assert host.socket("tcp://127.0.0.1:9300")
    s = host.service("elasticsearch")
    assert s.is_running
    assert s.is_enabled
